# Firefox Move Tab to Container

Firefox extension that handles moving a tab to a different container, created to solve [the long lasting Firefox issue #1511449](https://bugzilla.mozilla.org/show_bug.cgi?id=1511449) or at least make it bearable until the real solution is officially released.

Technically it opens the selected tab (or all selected tabs) in a container and then closes the original. This addon used to be named "Close Original Tab". The context menu item is `Quit Tab and Open in container` because I wanted "q" to be an activator key.

## Installation
Available on [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/move-to-container/) like any other extension.

## Usage
After installation there will be new position in tab's context menu entry activated with "q". Intended workflow:

1. right click a tab or a highlighted set of tabs
1. press "q" (as opposite to default "e" that triggers the native browser behaviour)
1. press the first letter of your container name

## Support
Just open an [issue](../../issues), I'll try to at least comment ASAP.

## Contributing
Merge requests welcome. Feel free to send one.

## Known issues
I don't know how to colour the SVG icons in context menu.
Originally I wanted my context menu entry to replace the native one but I can't seem to find a way to do it from within an extension.

## Project status
For now, I consider it done.
