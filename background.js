if (browser.contextualIdentities === undefined) throw new Error("Containers disabled");

browser.menus.create({
    id: 'no-container',
    type: 'normal',
    title: '&No Container',
    contexts: ['tab'],
});

browser.contextualIdentities.query({}).then(identities => {
    for (let i of identities) {
        browser.menus.create({
            id: i.cookieStoreId,
            type: 'normal',
            title: i.name,
            contexts: ['tab'],
            icons: {'16': i.iconUrl},
        });
    }

});

browser.menus.onClicked.addListener(closeOriginalTab);

async function getSelectedTabs() {
    return browser.tabs.query({
        highlighted: true,
        currentWindow: true,
    });
}

function reopenTabInContainerAndCloseOriginal(tab, cookieStoreId) {
    let opts = {
        url: tab.url,
        cookieStoreId: cookieStoreId,
        index: tab.index + 1,
        pinned: tab.pinned,
    };
    if (cookieStoreId === 'no-container') delete opts.cookieStoreId;
    browser.tabs.create(opts);
    browser.tabs.remove(tab.id);
}

async function closeOriginalTab(menuItem, tab) {
    let tabs = await getSelectedTabs();
    let clickedOnSelectedTab = false;
    for (t of tabs) if (t.id === tab.id) clickedOnSelectedTab = true;
    if (!clickedOnSelectedTab) tabs = [tab];
    tabs.map(t => reopenTabInContainerAndCloseOriginal(t, menuItem.menuItemId));
}
